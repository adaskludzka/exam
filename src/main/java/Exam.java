import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Exam {

    boolean exit = true;

    boolean exitFromWifi = true;

    Scanner sc = new Scanner(System.in);

    List<String> filaments = new LinkedList<>();

    List<String> heaterTemperature = new LinkedList<>();

    List<String> projects = new LinkedList<>();

    List<String> connectWifi = new LinkedList<>();

    List<String> tableTemperature = new LinkedList<>();


    public void run() {

        while(exit){

            System.out.println("Uruchomiono 3DPrinter. Wybierz opcję:");

            System.out.println("1. Zmień filament. \n" +
                               "2. Zmień temperaturę grzałki. \n" +
                               "3. Zmień temperaturę stołu. \n" +
                               "4. Wybierz projekt. \n" +
                               "5. Połącz z WIFI. \n" +
                               "6. Sprawdź zmiany.\n" +
                               "7. Informacje o drukarce.");

            String decision = sc.next();

            if (decision.equals("1")) {
                filamentChangeing();
            }

            if (decision.equals("2")) {
                heaterTempChangeing();
            }

            if (decision.equals("3")){
                tableTempChangeing();
            }

            if(decision.equals("4")){
                chooseingProject();
            }

            if(decision.equals("5")){
                connectingToWifi();
            }

            if(decision.equals("6")){
                checkingChanges();
            }

            if(decision.equals("7")){
                printerInfo();
            }

        }


    }

    private void printerInfo() {

        System.out.println("Nazwa: 3DPrinter \n" +
                           "ID: 124567 \n" +
                           "Technologia druku: FDM \n" +
                           "Prędkość druku: 200mm/s \n" +
                           "Wznawianie druku: Tak \n" +
                           "Wbudowana kamera: Nie");

    }

    private void tableTempChangeing() {

        System.out.println("Obecna temperatura:0°C. Wpisz nową wartość.");

        String tempOfTable = sc.next();

        System.out.println("Obecna temperatura:" + tempOfTable + "°C");

        tableTemperature.add(tempOfTable);
    }



    private void checkingChanges() {

        if(filaments.isEmpty()){
            System.out.println("Brak zmian.");
        }else{
            System.out.println(filaments);
        }

        if(heaterTemperature.isEmpty()) {
            System.out.println("Brak zmian.");
        }else {
            System.out.println("Temperatura grzałki" + heaterTemperature + "°C");
        }

        if(projects.isEmpty()){
            System.out.println("Brak zmian.");
        }else{
            System.out.println(projects);
        }

        if(connectWifi.isEmpty()){
            System.out.println("Brak zmian.");
        }else{
            System.out.println(connectWifi);
        }

        if(tableTemperature.isEmpty()){
            System.out.println("Brak zmian.");
        }else{
            System.out.println("Temperatura stołu" + tableTemperature + "°C");
        }



    }


    private void connectingToWifi() {

        System.out.println("Brak połączenia. Czy chcesz połączyć z WIFI?");

        System.out.println("1. Tak. \n" +
                           "2. Nie. \n");

        String wifi = sc.next();

        if (wifi.equals("1")) {
            System.out.println("Połączono z WIFI.");
            connectWifi.add("Połączono z WIFI.");
        } else {
            System.out.println("Brak połączenia.");
            connectWifi.add("Rozłączono z WIFI.");
        }


    }

    private void chooseingProject() {

        System.out.println("Wybierz nowy projekt:");

        System.out.println("1. Łódka (PLA) \n" +
                           "2. Łódka (ABS) \n" +
                           "3. Łódka (PETG) \n" +
                           "4. Brelok (PLA) \n" +
                           "5. Brelok (ABS) \n" +
                           "6. Brelok (PETG) \n" +
                           "7. Wazon (PLA) \n" +
                           "8. Wazon (ABS) \n" +
                           "9. Wazon (PETG) \n" +
                           "10. Bransoletka (PLA) \n" +
                           "11. Podkładka pod kubek (ABS)");

        String project = sc.next();

        if (project.equals("1")) {
            System.out.println("Wybrałeś Łódka (PLA).");
            projects.add("Projekt: Łódka (PLA) ");
        }

        if (project.equals("2")){
            System.out.println("Wybrałeś Łódka (ABS).");
            projects.add("Projekt: Łódka (ABS) ");
        }

        if(project.equals("3")){
            System.out.println("Wybrałeś Łódka (PETG).");
            projects.add("Projekt: Łódka (PETG) ");
        }

        if (project.equals("4")) {
            System.out.println("Wybrałeś Brelok (PLA).");
            projects.add("Projekt: Brelok (PLA) ");
        }

        if (project.equals("5")){
            System.out.println("Wybrałeś Brelok (ABS).");
            projects.add("Projekt: Brelok (ABS) ");
        }

        if(project.equals("6")){
            System.out.println("Wybrałeś Brelok (PETG).");
            projects.add("Projekt: Brelok (PETG) ");
        }

        if (project.equals("7")) {
            System.out.println("Wybrałeś Wazon (PLA).");
            projects.add("Projekt: Wazon (PLA) ");
        }

        if (project.equals("8")){
            System.out.println("Wybrałeś Wazon (ABS).");
            projects.add("Projekt: Wazon (ABS) ");
        }

        if(project.equals("9")){
            System.out.println("Wybrałeś Wazon (PETG).");
            projects.add("Projekt: Wazon (PETG) ");
        }

        if (project.equals("10")) {
            System.out.println("Wybrałeś Bransoletka (PLA).");
            projects.add("Projekt: Bransoletka (PLA) ");
        }

        if (project.equals("11")){
            System.out.println("Wybrałeś Podkładka pod kubek (ABS).");
            projects.add("Projekt: Podkładka pod kubek (ABS) ");
        }


    }

    private void heaterTempChangeing() {

        System.out.println("Obecna temperatura:0°C. Wpisz nową wartość.");

        String temp = sc.next();

        System.out.println("Obecna temperatura:" + temp + "°C");

        heaterTemperature.add(temp);
    }

    private void filamentChangeing() {

        System.out.println("Wybierz rodzaj filamentu:");

        System.out.println("1. PLA. \n" +
                           "2. ABS. \n" +
                           "3. PETG.");

        String chooseFilament = sc.next();

        if (chooseFilament.equals("1")) {
            System.out.println("Wybrałeś PLA.");
            filaments.add("Filament: PLA");
        }

        if (chooseFilament.equals("2")){
            System.out.println("Wybrałeś ABS.");
            filaments.add("Filament: ABS");
        }

        if(chooseFilament.equals("3")){
            System.out.println("Wybrałeś PETG.");
            filaments.add("Filament: PETG");
        }

    }

}
